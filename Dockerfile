FROM openjdk:21-slim
EXPOSE 8080
ADD target/delivery-management-0.0.1-SNAPSHOT.jar delivery-management.jar
ENTRYPOINT ["java","-jar","/delivery-management.jar"]