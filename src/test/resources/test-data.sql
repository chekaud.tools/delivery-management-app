CREATE TABLE IF NOT EXISTS users (
                                     id int8 PRIMARY KEY,
                                     name VARCHAR(255),
                                     email VARCHAR(255),
                                     phone VARCHAR(255),
                                     address VARCHAR(255),
                                     created_at TIMESTAMP,
                                     updated_at TIMESTAMP,
                                     status VARCHAR(64)
);

CREATE TABLE IF NOT EXISTS reservations (
                                            id VARCHAR(64) PRIMARY KEY,
                                            user_id int8,
                                            slot_start TIMESTAMP,
                                            slot_end TIMESTAMP,
                                            delivery_type VARCHAR(255),
                                            address VARCHAR(255),
                                            note VARCHAR(255),
                                            created_at TIMESTAMP ,
                                            updated_at TIMESTAMP,
                                            status VARCHAR(64),
                                            FOREIGN KEY (user_id) REFERENCES users(id)
);


INSERT INTO users (id, name, email, phone, address, created_at, updated_at, status)
VALUES
    (1  , 'admin', 'admin@gmail.com', '1234567890', 'admin address', '2020-01-01 00:00:00', '2020-01-01 00:00:00', 'ENABLED'),
    (2  , 'user1', 'user1@gmail.com', '1234567890', 'user1 address', '2020-01-01 00:00:00', '2020-01-01 00:00:00', 'ENABLED'),
    (3  , 'user 2', 'user2@gmail.com', '1234567890', 'user2 address', '2020-01-01 00:00:00', '2020-01-01 00:00:00', 'ENABLED'),
    (4  , 'user 3', 'user3@gmail.com', '1234567890', 'user3 address', '2020-01-01 00:00:00', '2020-01-01 00:00:00', 'ENABLED'),
    (5  , 'user 4', 'user4@gmail.com', '1234567890', 'user4 address', '2020-01-01 00:00:00', '2020-01-01 00:00:00', 'ENABLED');


INSERT INTO public.reservations (id, user_id, note, address, created_at, updated_at, slot_start, slot_end, delivery_type, status)
VALUES ('6ecc5de8-f7d4-454a-a4ff-c7e600da087b', 1, 'string', 'string', '2024-03-04 18:17:34.951661', null, '2024-03-04 18:08:36.612000', '2024-03-04 18:08:36.612000', 'DELIVERY', 'CREATED');
