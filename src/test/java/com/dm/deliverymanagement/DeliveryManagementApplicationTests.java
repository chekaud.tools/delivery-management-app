package com.dm.deliverymanagement;

import com.dm.deliverymanagement.domain.User;
import com.dm.deliverymanagement.domain.enums.DeliveryType;
import com.dm.deliverymanagement.dto.SlotReservationDto;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.kafka.test.context.EmbeddedKafka;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Objects;


@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@EmbeddedKafka(partitions = 1, brokerProperties = { "listeners=PLAINTEXT://localhost:9092", "port=9092" })
class DeliveryManagementApplicationTests extends SharedTestsPart {

    /**
     * Users Tests
     */
    @Test
    @Order(1)
    void searchForUsers() {
        webTestClient.get()
                .uri("/api/v1/users/all")
                .exchange()
                .expectStatus().isOk()
                .expectBodyList(User.class)
                .hasSize(5);
    }

    @Test
    @Order(2)
    void searchForUser1() {
        Objects.requireNonNull(webTestClient.get()
                .uri("/api/v1/users/" + user.getId())
                .exchange()
                .expectStatus().isOk()
                .expectBody(User.class)
                .value(result -> result.getId().equals(user.getId())));
    }

    /**
     * Reservations Tests
     */

    @Test
    @Order(3)
    void searchForDeliveryTypes() {
        webTestClient.get()
                .uri("/api/v1/reservations/delivery-types")
                .exchange()
                .expectStatus().isOk()
                .expectBody(List.class)
                .value(result -> result.containsAll(List.of(DeliveryType.DELIVERY.name(),
                        DeliveryType.DRIVE.name(),
                        DeliveryType.DELIVERY_ASAP.name(),
                        DeliveryType.DELIVERY_TODAY.name())));
    }

    @Test
    @Order(4)
    void reserveSlotTest() {
        Instant now = Instant.now();
        Instant future = now.plus(15, ChronoUnit.HOURS);
        SlotReservationDto request = SlotReservationDto.builder()
                .userId(user.getId())
                .slotStart(future)
                .slotEnd(future.plus(2, ChronoUnit.HOURS))
                .address("Test Address")
                .deliveryType("DELIVERY")
                .note("Test Note")
                .build();
        webTestClient.post()
                .uri("/api/v1/reservations/"+user.getId()+"/reserve")
                .bodyValue(request)
                .exchange()
                .expectStatus().isOk()
                .expectBody(SlotReservationDto.class)
                .value(result -> result.getUserId().equals(user.getId()))
                .value(result -> result.getAddress().equals(request.getAddress()))
                .value(result -> result.getDeliveryType().equals(request.getDeliveryType()))
                .value(result -> result.getNote().equals(request.getNote()));
    }

}
