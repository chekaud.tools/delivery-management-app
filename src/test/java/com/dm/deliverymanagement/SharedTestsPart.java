package com.dm.deliverymanagement;

import com.dm.deliverymanagement.domain.User;
import com.dm.deliverymanagement.domain.enums.DeliveryType;
import com.dm.deliverymanagement.domain.enums.UserStatus;
import com.dm.deliverymanagement.dto.SlotReservationDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.springframework.test.web.reactive.server.WebTestClient;
import org.testcontainers.containers.PostgreSQLContainer;

import java.time.Instant;

public abstract class SharedTestsPart {

    @Autowired
    protected WebTestClient webTestClient;

    protected static final PostgreSQLContainer<?> postgreSQLContainer = new PostgreSQLContainer<>("postgres:16.1-bullseye")
            .withDatabaseName("integration-tests-db").withUsername("username").withPassword("password");


    // This reservation is created in the database
    static SlotReservationDto reservation = SlotReservationDto.builder()
            .id("6ecc5de8-f7d4-454a-a4ff-c7e600da087b")
            .userId(1L)
            .note("note")
            .address("address")
            .slotStart(Instant.now())
            .slotEnd(Instant.now())
            .deliveryType(DeliveryType.DELIVERY.name())
            .build();

    static User user = User.builder()
            .id(1L)
            .address("admin address")
            .email("email")
            .status(UserStatus.ENABLED)
            .build();

    static {
        postgreSQLContainer.start();
    }

    @DynamicPropertySource
    static void setProperties(DynamicPropertyRegistry dynamicPropertyRegistry) {
        dynamicPropertyRegistry.add("spring.r2dbc.url", () -> postgreSQLContainer.getJdbcUrl().replace("jdbc", "r2dbc"));
        dynamicPropertyRegistry.add("spring.r2dbc.username", postgreSQLContainer::getUsername);
        dynamicPropertyRegistry.add("spring.r2dbc.password", postgreSQLContainer::getPassword);
        dynamicPropertyRegistry.add("spring.liquibase.url", postgreSQLContainer::getJdbcUrl);
        dynamicPropertyRegistry.add("spring.liquibase.user", postgreSQLContainer::getUsername);
        dynamicPropertyRegistry.add("spring.liquibase.password", postgreSQLContainer::getPassword);
    }


}
