package com.dm.deliverymanagement.domain;

import com.dm.deliverymanagement.domain.enums.UserStatus;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.domain.Persistable;
import org.springframework.data.relational.core.mapping.Table;

import java.time.Instant;

@Data
@Table("users")
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class User  implements Persistable<Long> {

    @Id
    private Long id;
    private String name;
    private String email;
    private String phone;
    private String address;
    private Instant createdAt;
    private Instant updatedAt;
    private UserStatus status;

    @Override
    public boolean isNew() {
        return true;
    }
}
