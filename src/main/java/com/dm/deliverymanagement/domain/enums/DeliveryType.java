package com.dm.deliverymanagement.domain.enums;

import lombok.Getter;

@Getter
public enum DeliveryType {
    DRIVE("DRIVE"),
    DELIVERY("DELIVERY"),
    DELIVERY_TODAY("DELIVERY_TODAY"),
    DELIVERY_ASAP("DELIVERY_ASAP");

    private final String status;

    DeliveryType(String status) {
        this.status = status;
    }
}
