package com.dm.deliverymanagement.domain.enums;

import lombok.Getter;

@Getter
public enum UserStatus {
    ENABLED("ENABLED"),
    DISABLED("DISABLED");

    private final String status;

    UserStatus(String status) {
        this.status = status;
    }
}
