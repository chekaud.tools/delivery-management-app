package com.dm.deliverymanagement.api;

import com.dm.deliverymanagement.exception.NotFoundException;
import com.dm.deliverymanagement.exception.ServiceException;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.reactive.result.method.annotation.ResponseEntityExceptionHandler;

import java.util.Map;
import static com.dm.deliverymanagement.api.ErrorCodes.INTERNAL_SERVER_ERROR;


@ControllerAdvice
@Slf4j
public class ExceptionHandlers extends ResponseEntityExceptionHandler {

    private static final String LOG_ERROR = "An unexpected internal server error occured";

    @ExceptionHandler(Throwable.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ResponseBody
    public ResponseError handleThrowable(final Throwable ex) {
        log.error(LOG_ERROR, ex);
        if (ex instanceof ServiceException serviceException) {
            return new ResponseError(serviceException.getCode(), ex.getMessage());
        }
        return new ResponseError(INTERNAL_SERVER_ERROR, ex.getMessage());
    }



    @ExceptionHandler({ServiceException.class})
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public ResponseError handleServiceException(
            ServiceException ex, WebRequest request) {
        return new ResponseError(ex.getCode(), ex.getMessage());
    }

    @ExceptionHandler({NotFoundException.class})
    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ResponseBody
    public ResponseError handleNotFoundException(
            NotFoundException ex, WebRequest request) {
        return new ResponseError(ex.getCode(), ex.getMessage());
    }

    @Data
    @Builder
    @AllArgsConstructor
    public static class ResponseError {
        private final String code;
        private String message;
        private Map<?, ?> metadata;

        public ResponseError(String code, String message) {
            this.code = code;
            this.message = message;
        }
    }
}
