package com.dm.deliverymanagement.api;

public class ErrorCodes {

    public static final String INVALID_DATA = "INVALID_DATA";
    public static final String INTERNAL_SERVER_ERROR = "INTERNAL_SERVER_ERROR";

    private ErrorCodes() {
        throw new IllegalStateException("Utility Class");
    }
}
