package com.dm.deliverymanagement.api.controller;

import com.dm.deliverymanagement.dto.ReservationStatusDto;
import com.dm.deliverymanagement.dto.ReserveDto;
import com.dm.deliverymanagement.dto.SearchCriteria;
import com.dm.deliverymanagement.dto.SlotReservationDto;
import com.dm.deliverymanagement.dto.UpdateReservationDto;
import com.dm.deliverymanagement.exception.ServiceException;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.hateoas.EntityModel;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;
import java.util.UUID;

public interface SlotReservationController {

    @Operation(summary = "Get all reservations by user id and status")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Reservations found"),
            @ApiResponse(responseCode = "500", description = "Something went wrong")})
    @PostMapping("/user/{id}/status")
    Flux<SlotReservationDto> findAllReservationsByUserIdAndStatus(@PathVariable Long id, @RequestBody SearchCriteria criteria) ;

    @Operation(summary = "Get a reservation")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Reservation found"),
            @ApiResponse(responseCode = "404", description = "Reservation not found")})
    @GetMapping("/{id}")
    Mono<SlotReservationDto> getReservation(@PathVariable(name = "id") String id) ;

    @Operation(summary = "Get all reservations by user id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Reservations found"),
            @ApiResponse(responseCode = "500", description = "Something went wrong")})
    @GetMapping("/user/{id}")
    Flux<EntityModel<SlotReservationDto>> getReservationsByUserId(@PathVariable(name = "id") Long id);

    @Operation(summary = "Get possible delivery types for a reservation")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Delivery types found"),
            @ApiResponse(responseCode = "500", description = "Something went wrong")})
    @GetMapping("/delivery-types")
    Mono<List<String>> getDeliveryTypes();


    @Operation(summary = "Reserve a slot for delivery or drive through")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Reservation created"),
            @ApiResponse(responseCode = "400", description = "Invalid input")})
    @PostMapping("/{id}/reserve")
    Mono<SlotReservationDto> reserveSlot(@RequestBody ReserveDto request, @PathVariable(name = "id") Long id) throws ServiceException;

    @Operation(summary = "Cancel a reservation")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Reservation canceled"),
            @ApiResponse(responseCode = "404", description = "Reservation not found")})
    @PostMapping("/{userId}/cancel/{id}")
    Mono<SlotReservationDto> cancelReservation(@PathVariable(name = "id") String id, @PathVariable(name = "userId") Long userId);

    @Operation(summary = "Update a reservation status")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Reservation updated"),
            @ApiResponse(responseCode = "400", description = "Invalid input"),
            @ApiResponse(responseCode = "404", description = "Reservation not found")})
    @PostMapping("/{id}/update/status")
    Mono<SlotReservationDto> updateReservationStatus(@PathVariable(name = "id") String id, @RequestBody UpdateReservationDto request) ;

    @Operation(summary = "Get reservation status")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "The reservation status"),
            @ApiResponse(responseCode = "500", description = "Something went wrong")})
    @GetMapping("/{id}/status")
    Mono<ReservationStatusDto> getStatus(@PathVariable(name = "id") UUID id);
}
