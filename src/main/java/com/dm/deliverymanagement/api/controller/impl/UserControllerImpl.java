package com.dm.deliverymanagement.api.controller.impl;


import com.dm.deliverymanagement.api.controller.UserController;
import com.dm.deliverymanagement.domain.User;
import com.dm.deliverymanagement.service.UserService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/api/v1/users")
@AllArgsConstructor
public class UserControllerImpl implements UserController {

    private final UserService userService;

    @GetMapping("/all")
    @Override
    public Flux<User> getAllUsers() {
        return userService.getAllUsers();
    }

    @GetMapping("/{id}")
    @Override
    public Mono<User> getUserById(@PathVariable(name = "id") Long id) {
        return userService.getById(id);
    }

}
