package com.dm.deliverymanagement.api.controller.impl;

import com.dm.deliverymanagement.domain.enums.ReservationStatus;
import com.dm.deliverymanagement.dto.ReservationStatusDto;
import com.dm.deliverymanagement.dto.ReserveDto;
import com.dm.deliverymanagement.dto.SearchCriteria;
import com.dm.deliverymanagement.dto.SlotReservationDto;
import com.dm.deliverymanagement.dto.UpdateReservationDto;
import com.dm.deliverymanagement.dto.mapper.SlotReservationMapper;
import com.dm.deliverymanagement.exception.ServiceException;
import com.dm.deliverymanagement.service.SlotReservationService;
import com.dm.deliverymanagement.api.controller.SlotReservationController;
import lombok.AllArgsConstructor;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.Link;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;
import java.util.UUID;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;


@RestController
@RequestMapping("/api/v1/reservations")
@AllArgsConstructor
public class SlotReservationControllerImpl implements SlotReservationController {

    private final SlotReservationService slotReservationService;
    private final SlotReservationMapper slotReservationMapper;

    @Override
    @PostMapping("/user/{id}/status")
    public Flux<SlotReservationDto> findAllReservationsByUserIdAndStatus(@PathVariable(name = "id") Long id, @RequestBody SearchCriteria criteria) {
        return slotReservationService.findAllReservationsByUserIdAndStatus(id, ReservationStatus.valueOf(criteria.getStatus())).map(slotReservationMapper::toDTO);
    }

    @GetMapping("/{id}")
    @Override
    public Mono<SlotReservationDto> getReservation(@PathVariable(name = "id") String id) {
        return slotReservationService.getReservation(id).map(slotReservationMapper::toDTO);
    }

    @GetMapping("/user/{id}")
    @Override
    public Flux<EntityModel<SlotReservationDto>> getReservationsByUserId(@PathVariable(name = "id") Long id) {
        return slotReservationService.findAllReservationsByUserId(id)
                .map(slotReservationMapper::toDTO)
                .flatMap(dto -> addLinksToReservationDetails(dto, dto.getId(), id));
    }

    @GetMapping("/delivery-types")
    @Override
    public Mono<List<String>> getDeliveryTypes() {
        return slotReservationService.getDeliveryTypes();
    }


    @PostMapping("/{id}/reserve")
    @Override
    public Mono<SlotReservationDto> reserveSlot(@RequestBody ReserveDto request, @PathVariable(name = "id") Long id) throws ServiceException {
        return slotReservationService.reserveSlot(request, id).map(slotReservationMapper::toDTO);
    }

    @PostMapping("/{userId}/cancel/{id}")
    @Override
    public Mono<SlotReservationDto> cancelReservation(@PathVariable(name = "id") String id, @PathVariable(name = "userId") Long userId) {
        return slotReservationService.cancelReservation(id,userId).map(slotReservationMapper::toDTO);
    }

    @PostMapping("/{id}/update/status")
    @Override
    public Mono<SlotReservationDto> updateReservationStatus(@PathVariable(name = "id") String id, @RequestBody UpdateReservationDto request) {
        return slotReservationService.updateReservationStatus(id, request).map(slotReservationMapper::toDTO);
    }

    @GetMapping("/{id}/status")
    @Override
    public Mono<ReservationStatusDto> getStatus(@PathVariable(name = "id") UUID id) {
        return slotReservationService.getStatus(id).map(ReservationStatusDto::new);
    }

    /**
     * Add links to the response details
     * @param model the response details
     * @param reservationId the reservation id
     * @param <T> the type of the response details
     * @return the response details with links
     */
    private <T> Mono<EntityModel<T>> addLinksToReservationDetails(T model, String reservationId, Long userId) {
        Mono<Link> cancelLink = Mono.just(linkTo(methodOn(SlotReservationController.class).cancelReservation(reservationId, userId)).withRel("cancel"));
        Mono<Link> selfLink = Mono.just(linkTo(methodOn(SlotReservationController.class).getReservation(reservationId)).withRel("get"));
        return Mono.zip(cancelLink, selfLink)
                .map(links -> EntityModel.of(model, List.of(links.getT1(), links.getT2())));
    }

}
