package com.dm.deliverymanagement.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.time.Instant;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ReserveDto {

    private Long userId;
    private Instant day;
    private Instant slotStart;
    private Instant slotEnd;
    private String address;
    private String phone;
    private String note;
    private String deliveryType;

}
