package com.dm.deliverymanagement.dto.mapper;

import org.mapstruct.Mapping;
import com.dm.deliverymanagement.domain.SlotReservation;
import com.dm.deliverymanagement.dto.ReserveDto;
import com.dm.deliverymanagement.dto.SlotReservationDto;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface SlotReservationMapper {
    SlotReservationDto toDTO(SlotReservation slotReservation);

    @Mapping(target = "id", ignore = true)
    @Mapping(target = "createdAt", ignore = true)
    @Mapping(target = "updatedAt", ignore = true)
    @Mapping(target = "status", ignore = true)
    SlotReservation fromMakeReservationRequest(ReserveDto request);
}
