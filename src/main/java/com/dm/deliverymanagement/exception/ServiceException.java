package com.dm.deliverymanagement.exception;

import lombok.Getter;

@Getter
public class ServiceException extends Exception {

    private final String code;

    public ServiceException(String code, String errorMessage) {
        super(errorMessage);
        this.code = code;
    }

    public ServiceException(String code, String errorMessage, Throwable cause) {
        super(errorMessage, cause);
        this.code = code;
    }

}
