package com.dm.deliverymanagement.exception;

import lombok.Getter;

@Getter
public class NotFoundException extends Exception {
    private final String code;

    public NotFoundException(String code, String errorMessage) {
        super(errorMessage);
        this.code = code;
    }

}