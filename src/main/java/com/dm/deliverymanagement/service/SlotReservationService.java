package com.dm.deliverymanagement.service;

import com.dm.deliverymanagement.domain.SlotReservation;
import com.dm.deliverymanagement.domain.enums.ReservationStatus;
import com.dm.deliverymanagement.dto.ReserveDto;
import com.dm.deliverymanagement.dto.UpdateReservationDto;
import com.dm.deliverymanagement.exception.ServiceException;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;
import java.util.UUID;

public interface SlotReservationService {
    Flux<SlotReservation> findAllReservationsByUserId(Long userId);

    Flux<SlotReservation> findAllReservationsByUserIdAndStatus(Long userId, ReservationStatus status);

    Mono<SlotReservation> getReservation(String id);

    Mono<List<String>> getDeliveryTypes();

    Mono<SlotReservation> cancelReservation(String id, Long userId);

    Mono<SlotReservation> updateReservationStatus(String reservationId, UpdateReservationDto request);

    Mono<String> getStatus(UUID id);

    Mono<SlotReservation> reserveSlot(ReserveDto request, Long id) throws ServiceException;
}
