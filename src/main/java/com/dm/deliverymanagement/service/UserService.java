package com.dm.deliverymanagement.service;

import com.dm.deliverymanagement.domain.User;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface UserService {
    Flux<User> getAllUsers();

    Mono<User> getById(Long id);
}
