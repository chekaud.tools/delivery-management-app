package com.dm.deliverymanagement.service.impl;

import com.dm.deliverymanagement.domain.SlotReservation;
import com.dm.deliverymanagement.domain.enums.DeliveryType;
import com.dm.deliverymanagement.domain.enums.ReservationStatus;
import com.dm.deliverymanagement.dto.ReserveDto;
import com.dm.deliverymanagement.dto.UpdateReservationDto;
import com.dm.deliverymanagement.dto.mapper.SlotReservationMapper;
import com.dm.deliverymanagement.exception.NotFoundException;
import com.dm.deliverymanagement.exception.ServiceException;
import com.dm.deliverymanagement.messaging.CreatedReservationsMessagesProducer;
import com.dm.deliverymanagement.repository.SlotReservationRepository;
import com.dm.deliverymanagement.service.SlotReservationService;
import lombok.AllArgsConstructor;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.Instant;
import java.time.ZoneId;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import static com.dm.deliverymanagement.api.ErrorCodes.INVALID_DATA;
import static com.dm.deliverymanagement.utils.AppConstants.HALF_HOUR;
import static com.dm.deliverymanagement.utils.AppConstants.ONE_HOUR;
import static com.dm.deliverymanagement.utils.AppConstants.RESERVATION_NOT_FOUND;
import static com.dm.deliverymanagement.utils.AppConstants.RESERVATION_NOT_FOUND_LABEL;
import static com.dm.deliverymanagement.utils.AppConstants.ZONE_ID;
import static com.dm.deliverymanagement.utils.CacheConstants.CACHE_KEY;
import static com.dm.deliverymanagement.utils.CacheConstants.CACHE_RESERVATION;

@Service
@AllArgsConstructor
public class SlotReservationServiceImpl implements SlotReservationService {

    private final SlotReservationRepository slotReservationRepository;
    private final SlotReservationMapper slotReservationMapper;
    private final CreatedReservationsMessagesProducer createdReservationsMessagesProducer;

    @Override
    @Cacheable(cacheNames = CACHE_RESERVATION, key = "{" + CACHE_KEY + ", #userId}")
    public Flux<SlotReservation> findAllReservationsByUserId(Long userId) {
        return slotReservationRepository.findAllByUserId(userId)
                .switchIfEmpty(Mono.error(new NotFoundException(RESERVATION_NOT_FOUND, RESERVATION_NOT_FOUND_LABEL)));
    }

    @Override
    public Flux<SlotReservation> findAllReservationsByUserIdAndStatus(Long userId, ReservationStatus status) {
        return slotReservationRepository.findAllByUserIdAndStatus(userId, status)
                .switchIfEmpty(Mono.error(new NotFoundException(RESERVATION_NOT_FOUND, RESERVATION_NOT_FOUND_LABEL)));
    }

    @Override
    public Mono<SlotReservation> getReservation(String id) {
        return slotReservationRepository.findById(id)
                .switchIfEmpty(Mono.error(new NotFoundException(RESERVATION_NOT_FOUND, RESERVATION_NOT_FOUND_LABEL)));
    }

    @Override
    public Mono<List<String>> getDeliveryTypes() {
        List<String> stringValues = Arrays.stream(DeliveryType.values())
                .map(Enum::toString)
                .toList();
        return Mono.just(stringValues);
    }

    @Override
    @CacheEvict(cacheNames = CACHE_RESERVATION, key = "{" + CACHE_KEY + ", #userId}")
    public Mono<SlotReservation> cancelReservation(String id, Long userId) {
        return slotReservationRepository.findById(id)
                .flatMap(reservation -> {
                    reservation.setStatus(ReservationStatus.CANCELED);
                    return slotReservationRepository.save(reservation);
                });
    }

    @Override
    @CacheEvict(cacheNames = CACHE_RESERVATION, key = "{" + CACHE_KEY + ", #request.userId}")
    public Mono<SlotReservation> updateReservationStatus(String reservationId, UpdateReservationDto request) {
        return this.getReservation(reservationId).flatMap(reservation -> {
            reservation.setStatus(ReservationStatus.valueOf(request.getStatus()));
            return slotReservationRepository.save(reservation);
        });
    }

    @Override
    public Mono<String> getStatus(UUID id) {
        return slotReservationRepository.findById(id.toString()).map(SlotReservation::getStatus).map(ReservationStatus::getStatus);
    }

    @Override
    @CacheEvict(cacheNames = CACHE_RESERVATION, key = "{" + CACHE_KEY + ", #id}")
    public Mono<SlotReservation> reserveSlot(ReserveDto request, Long id) throws ServiceException {
        // check if the slots are valid
        checkSlotsValidity(request);
        // create the reservation and send a message to the kafka topic
        SlotReservation slotReservation = slotReservationMapper.fromMakeReservationRequest(request);
        slotReservation.setId(UUID.randomUUID().toString());
        slotReservation.setUserId(id);
        slotReservation.setStatus(ReservationStatus.CREATED);
        slotReservation.setCreatedAt(Instant.now());
        return slotReservationRepository.save(slotReservation).doOnNext(createdReservationsMessagesProducer::sendMessage);

    }

    /**
     * Check if the slots are valid
     *
     * @param reservation the reservation to check
     */
    private void checkSlotsValidity(ReserveDto reservation) throws ServiceException {
        Instant now = Instant.now();
        Instant slotStart = reservation.getSlotStart();
        Instant slotEnd = reservation.getSlotEnd();


        DeliveryType type = DeliveryType.valueOf(reservation.getDeliveryType().toUpperCase());

        // check if the slot start and end are provided
        if (slotStart == null || slotEnd == null) {
            throw new ServiceException(INVALID_DATA, "Slot start and end must be provided");
        }

        // check if the slot start is in the future
        if (!slotStart.isAfter(now)) {
            throw new ServiceException(INVALID_DATA, "Slot start must be in the future");
        }
        // check if the slot end is after the slot start
        if (!slotEnd.isAfter(slotStart)) {
            throw new ServiceException(INVALID_DATA, "Slot end must be after slot start");
        }

        // check the compatibility of the delivery type with the slots
        checkDeliveryTypeSlotsCompatibility(reservation, type, now, slotStart);

    }

    /**
     * Check if the delivery type is valid
     *
     * @param reservation the reservation to check
     * @param type        the delivery type
     */
    private void checkDeliveryTypeSlotsCompatibility(ReserveDto reservation, DeliveryType type, Instant now, Instant slotStart) throws ServiceException {
        switch (type) {
            case DELIVERY -> {

            }
            case DELIVERY_TODAY -> {
                if (!slotStart.atZone(ZoneId.of(ZONE_ID)).toLocalDate().isEqual(now.atZone(ZoneId.of(ZONE_ID)).toLocalDate())) {
                    throw new ServiceException(INVALID_DATA, "Delivery slot must be in the same day");
                }
            }
            case DELIVERY_ASAP -> {
                if (slotStart.isBefore(now.plusSeconds(ONE_HOUR))) {
                    throw new ServiceException(INVALID_DATA, "Delivery slot must be at least 1 hour from now");
                }
            }
            case DRIVE -> {
                if (slotStart.isBefore(now.plusSeconds(HALF_HOUR))) {
                    throw new ServiceException(INVALID_DATA, "Drive slot must be at least 30 minutes from now");
                }
            }
            default ->
                    throw new ServiceException(INVALID_DATA, "Unknown delivery type: " + reservation.getDeliveryType());
        }
    }
}
