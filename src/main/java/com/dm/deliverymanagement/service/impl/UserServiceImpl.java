package com.dm.deliverymanagement.service.impl;

import com.dm.deliverymanagement.domain.User;
import com.dm.deliverymanagement.exception.NotFoundException;
import com.dm.deliverymanagement.repository.UserRepository;
import com.dm.deliverymanagement.service.UserService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import static com.dm.deliverymanagement.utils.AppConstants.USER_NOT_FOUND;

@Service
@AllArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    @Override
    public Flux<User> getAllUsers() {
        return userRepository.findAll()
                .switchIfEmpty(Mono.error(new NotFoundException(USER_NOT_FOUND,"No users found")));
    }

    @Override
    public Mono<User> getById(Long id) {
        return userRepository.findById(id)
                .switchIfEmpty(Mono.error(new NotFoundException(USER_NOT_FOUND,"No users found")));
    }

}
