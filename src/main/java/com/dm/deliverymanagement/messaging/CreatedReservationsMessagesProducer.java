package com.dm.deliverymanagement.messaging;

import com.dm.deliverymanagement.domain.SlotReservation;
import lombok.AllArgsConstructor;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
public class CreatedReservationsMessagesProducer {

    private final static String CREATED_RESERVATION_TOPIC_NAME = "created-reservations";
    private final KafkaTemplate<String, SlotReservation> kafkaTemplate;

    public void sendMessage(SlotReservation slotReservation) {
        kafkaTemplate.send(CREATED_RESERVATION_TOPIC_NAME, slotReservation);
    }
}