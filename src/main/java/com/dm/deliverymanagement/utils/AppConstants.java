package com.dm.deliverymanagement.utils;

public class AppConstants {
    public static final  String RESERVATION_NOT_FOUND = "RESERVATION.NOT.FOUND";
    public static final  String RESERVATION_NOT_FOUND_LABEL = "No reservation found";
    public static final  String USER_NOT_FOUND = "USER.NOT.FOUND";
    public static final int ONE_HOUR = 3600;
    public static final int HALF_HOUR = 1800;
    public static final String ZONE_ID = "UTC";
    private AppConstants() {
        throw new IllegalStateException("Utility Class");
    }
}
