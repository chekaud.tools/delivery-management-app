package com.dm.deliverymanagement.repository;


import com.dm.deliverymanagement.domain.SlotReservation;
import com.dm.deliverymanagement.domain.enums.ReservationStatus;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;


@Repository
public interface SlotReservationRepository extends ReactiveCrudRepository<SlotReservation, String> {

    Flux<SlotReservation> findAllByUserId(Long userId);

    Flux<SlotReservation> findAllByUserIdAndStatus(Long userId, ReservationStatus status);
}
