# Delivery Management

This is a simple project where the user can ask for a delivery by choosing specific types and time slots.
Some Users can also update status of reservations

### Getting Started
First run `docker-compose up` to start the project.

### Usage
You can go to `localhost:8080/api/v1/reservations/delivery-types` to get the list of delivery types.
curl command: `curl localhost:8080/api/v1/reservations/delivery-types` in the terminal.

Once the delivery type is chosen, you can specify the delivery slot by providing the start and end times to reserve it:
```agsl
curl -X 'POST' \
  'http://localhost:8080/api/v1/reservation/1/reserve' \
  -H 'accept: */*' \
  -H 'Content-Type: application/json' \
  -d '{
  "userId": 1,
  "day": "2024-03-04T08:29:12.198Z",
  "slotStart": "2024-03-04T08:29:12.198Z",
  "slotEnd": "2024-03-04T08:29:12.198Z",
  "address": "address",
  "phone": "0679797979",
  "note": "Sample note",
  "deliveryType": "DELIVERY"
}'
```
You can check the status of your reservation by running the following command:
```agsl
curl -X 'GET' \
  'http://localhost:8080/api/v1/reservations/6ecc5de8-f7d4-454a-a4ff-c7e600da087b/status' \
  -H 'accept: */*'
```
You can cancel your reservation by running the following command:
```agsl
curl -X 'POST' \
'http://localhost:8080/api/v1/reservations/6ecc5de8-f7d4-454a-a4ff-c7e600da087b/update/status' \
-H 'accept: */*' \
-H 'Content-Type: application/json' \
-d '{
"status": "CANCELED"
}'
```

### Messaging
When you reserve your slot, a new reservation gets created and a message is sent to the `created-reservations` topic.
To see the messages coming in, you can run the following that enables you to listen to Kafka messages:
```
docker exec -ti kafka /opt/kafka/bin/kafka-console-consumer.sh \
  --bootstrap-server localhost:9092 \
  --topic created-reservations \
  --from-beginning
```


### TODO
- Deploy to k8s

- Secure the api
